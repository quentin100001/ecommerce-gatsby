import React from "react";
import { Link } from "gatsby";
import PropTypes from "prop-types"

export class NavItem extends React.Component {
    render() {
        return (
          <li className="nav__item">
              <Link to={this.props.to}>{this.props.children}</Link>
          </li>
        )
    }
}

NavItem.propTypes = {
    to: PropTypes.string.isRequired
};
