module.exports = {
  siteMetadata: {
    title: `La Garenne Pizza`,
    description: `Pizzas à emporter à La Garenne-Colombes.`,
    author: `@Quentin`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-netlify-cms`,
    `gatsby-transformer-remark`,
    `gatsby-plugin-react-leaflet`,
    `gatsby-source-instance-name-for-remark`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/img`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `category`,
        path: `${__dirname}/collections/categories`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pizza`,
        path: `${__dirname}/collections/pizzas`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `setting`,
        path: `${__dirname}/collections/settings`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `news`,
        path: `${__dirname}/collections/news`,
      },
    },
    {
      resolve: `gatsby-source-stripe`,
      options: {
        objects: ['Product', 'Sku'],
        secretKey: 'sk_test_JDyRTdlCT9rOSEnD7JUocP1s00snsCk9wq',
        downloadFiles: true,
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/img/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
