import React from "react";
import PropTypes from "prop-types"
import "../assets/sass/theme/_button.scss"

export default class Button extends React.Component{
    render() {
        return (
          <>
              <button data-tip={this.props.tooltip} className={`btn btn-${this.props.type} ${this.props.className}`} onClick={this.props.onClick}>
                  {this.props.children}
              </button>
          </>
        )
    }
}

Button.propTypes = {
    type: PropTypes.oneOf(["primary", "outline"]),
    onClick: PropTypes.func,
    className: PropTypes.string
};

Button.defaultProps = {
    className: "",
    onClick: ()=>{}
}

