import React from "react"
import { Header } from "../components/Header"
import { Content } from "../components/Content"
import { Section } from "../components/Section"
import { Footer } from "../components/Footer"
import { faPhone, faEnvelope, faMapMarkedAlt } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const contact = () => {
    return (
      <>
          <Header title="La Garenne Pizza"/>
          <Content>
              <Section title="Contact" darkMode={false} className="contact">
                  <h3><FontAwesomeIcon icon={faPhone} /> Telephone</h3>
                  <p>06 68 06 12 34</p>

                  <h3><FontAwesomeIcon icon={faEnvelope} /> Email</h3>
                  <p>lagarennepizza@gmail.com</p>

                  <h3><FontAwesomeIcon icon={faMapMarkedAlt} /> Adresse</h3>
                  <p>58 Avenue Foch (sauf jeudi) <br/>
                      Place des Champs Philippe (jeudi)
                      92250 La Garenne-Colombes
                      France</p>
              </Section>
          </Content>
          <Footer/>
      </>
    )
}

export default contact
