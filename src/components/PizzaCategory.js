import React from "react";
import PropTypes from "prop-types"

export class PizzaCategory extends React.Component{
    render() {
        return (
          <table className="menu__cat">
              <tbody>
              <tr className="menu__cat__header"><th colSpan={2}><h3>{this.props.name}</h3></th><th className="menu__cat__rotate">33cm</th><th className="menu__cat__rotate">46cm</th></tr>
              {this.props.children}
              </tbody>
          </table>
        )
    }
}

PizzaCategory.propTypes = {
    name: PropTypes.string.isRequired
};
