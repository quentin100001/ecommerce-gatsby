---
news_title: Les pizzas du mois de Mai
news_image: /assets/2020-05-05.jpg
news_description: >-
  **Bresaola :** Tomate, mozza, Bresaola, roquette, parmesan, huile olive. 12€\

  **Truffe noire :** crème, champignons, mozza, huile de truffe, pancetta, parmesan. 12€\

  **Pizza Alex :** Tomate, anchois, confit d'oignons, mozza de bufala, olives noires, basilic. 13€+


  \

  *Les pizzas du mois sont préparées en taille moyenne uniquement.*
---
