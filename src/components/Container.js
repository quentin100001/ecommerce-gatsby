import React from "react";
import PropTypes from "prop-types"

export class Container extends React.Component {
    render() {
        return (
          <div className={"container " + (this.props.className || "")}>
              {this.props.children}
          </div>
        )
    }
}

Container.propTypes = {
    className: PropTypes.string,
};
