import React from "react";
import PropTypes from "prop-types"

export class Pizza extends React.Component{
    render() {
        return (
          <tr><th>{this.props.name}</th><td>{this.props.ingredients}</td><td>{this.props.price33}€</td><td>{this.props.price46}€</td></tr>
        )
    }
}

Pizza.propTypes = {
    name: PropTypes.string.isRequired,
    ingredients: PropTypes.string.isRequired,
    price33: PropTypes.number.isRequired,
    price46: PropTypes.number.isRequired,
};
