import React from "react"
import { Header } from "../components/Header"
import { Content } from "../components/Content"
import { Section } from "../components/Section"
import { Footer } from "../components/Footer"
import { faChevronRight, faTimes } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Container } from "../components/Container"
import { Col } from "../components/Col"
import Button from "../components/Button"
import { loadStripe } from "@stripe/stripe-js"

const checkout = () => {
    const redirectToCheckout = async event => {
        event.preventDefault()
        const stripe = await loadStripe("pk_test_FQs079xahXT2BGBZXe9TWdNW00LZZi9vuz")
        const { error } = await stripe.redirectToCheckout({
            lineItems: [{ price: "price_1GqMDIBMU3aceoMqT2KqLf9c", quantity: 1 }],
            mode: 'payment',
            successUrl: `http://localhost:8000/checkout/`,
            cancelUrl: `http://localhost:8000/`,
        })
        if (error) {
            console.warn("Error:", error)
        }
    }

    const defaultBracket = (typeof window !== 'undefined' ? (localStorage.getItem('bracket') ? JSON.parse(localStorage.getItem('bracket')) : {}) : {});
    let total = 0;
    return (
      <>
          <Header title="La Garenne Pizza"/>
          <Content>
              <Section title="Commander en ligne" darkMode={false}>
                  <table className="checkout">
                      <thead>
                        <tr>
                            <th />
                            <th />
                            <th>Produit</th>
                            <th>Prix</th>
                            <th>Quantité</th>
                            <th>Sous-total</th>
                        </tr>
                      </thead>
                      <tbody>
                      {Object.values(defaultBracket).map((value, index) => {
                          total += value.price*value.stack;
                          return (
                            <tr key={index}>
                                <td><FontAwesomeIcon icon={faTimes}/></td>
                                <td><img src={value.img} alt="pizza"/></td>
                                <td>{value.name}</td>
                                <td>{value.price}</td>
                                <td>{value.stack}</td>
                                <td>{value.price*value.stack}€</td>
                            </tr>
                          )
                      })}
                      </tbody>
                  </table>
                  <Container className="totals">
                      <Col size={50} />
                      <Col size={50}>
                          <h3>Total de la commande</h3>
                          <table>
                              <tbody>
                                <tr>
                                    <td>Total</td>
                                    <td>{total}€</td>
                                </tr>
                              </tbody>
                          </table>
                          <Button type="primary" onClick={redirectToCheckout}>Payer la commande <FontAwesomeIcon icon={faChevronRight}/></Button>
                      </Col>
                  </Container>
              </Section>
          </Content>
          <Footer/>
      </>
    )
}
export default checkout
