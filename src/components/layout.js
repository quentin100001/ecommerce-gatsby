import React from "react"
import { navigate } from "gatsby"
import AwesomeSlider from 'react-awesome-slider';
import AwesomeSliderStyles from 'react-awesome-slider/src/styles';
import PropTypes from "prop-types"

import { Header } from "./Header"
import "../assets/sass/style.scss"
import { Content } from "./Content"
import base from "../assets/img/IMG-20190623-WA0002.png";
import Button from "./Button"
import { Section } from "./Section"
import { Container } from "./Container"
import { Col } from "./Col"
import { Gallery } from "./Gallery"
import img1 from "../assets/img/IMG-20190623-WA0002.png"
import { Footer } from "./Footer"
import "leaflet/dist/leaflet.css"

const Layout = () => {
    return (
        <>
            <Header title="La Garenne Pizza" />
            <Content>
                <AwesomeSlider className="content__slider" bullets={false} cssModule={AwesomeSliderStyles} >
                    <div className="slider__item" data-src={base}>
                        <h2>Frais, rapide & délicieux</h2>
                        <p>Nos pizzas sont réalisées avec des ingrédients frais et de saison</p>
                        <Button type="outline" onClick={() => navigate("/shop")}>Commander</Button>
                    </div>
                    <div data-src="https://external-preview.redd.it/GOkP8onbuyjGmN9Rc8Que5mw21CdSw6OuXpAKUuE6-4.jpg?auto=webp&s=2bc0e522d1f2fa887333286d557466b2be00fa5e" />
                </AwesomeSlider>
                <Section title="Planning du camion">
                    <p className="content__section__caption">Nous sommes présent à la garenne-colombes à deux adresses :</p>
                    <Container className="homepage__section">
                        <Col size={50}>
                            <h3>Place de l'église</h3>
                            <p className="content__section__caption">58 Avenue Foch, 92250 La Garenne-Colombes</p>
                            <Container className="content__section__text">
                                <Col size={50}>Mardi</Col>
                                <Col size={50}>17h - 21h</Col>
                            </Container>
                            <Container className="content__section__text">
                                <Col size={50}>Mercredi</Col>
                                <Col size={50}>17h - 21h</Col>
                            </Container>
                            <Container className="content__section__text">
                                <Col size={50}>Vendredi</Col>
                                <Col size={50}>17h - 21h</Col>
                            </Container>
                            <Container className="content__section__text">
                                <Col size={50}>Samedi</Col>
                                <Col size={50}>17h - 21h</Col>
                            </Container>
                        </Col>
                        <Col size={50}>
                            <h3>Champs philippe</h3>
                            <p className="content__section__caption">Place des Champs Philippe, 92250 La Garenne-Colombes</p>
                            <Container className="content__section__text">
                                <Col size={50}>Jeudi</Col>
                                <Col size={50}>17h - 21h</Col>
                            </Container>
                        </Col>
                    </Container>
                </Section>
                <Section title="Galerie" noBorder={true}>
                    <p className="content__section__caption">Découvrez notre camion en images</p>
                    <Gallery>
                        <div className="gallery__item"><img src={img1} alt="img1"/></div>
                        <div className="gallery__item"><img src={img1} alt="img1"/></div>
                        <div className="gallery__item"><img src={img1} alt="img1"/></div>
                        <div className="gallery__item"><img src={img1} alt="img1"/></div>
                    </Gallery>
                </Section>
                <Section title="Un menu varié" darkMode={false}>
                    <div className="text-center">
                        <p>Le menu change chaque année.</p>
                        <p>Mais notre spécialité ce sont 3 nouvelles pizzas par mois.
                            Qui laissent place, le mois suivant, à 3 nouvelles.</p>
                        <p>Certaines sont même imaginées par les clients !</p>
                        <p> Alors lancez nous un défi :  imaginez la prochaine pizza du mois et donnez lui votre prénom.
                            Puis retrouvez la au menu pour une durée limitée.</p>
                        <p>Soyez créatifs et originaux.</p>
                        <p>Et gagnez la 2 fois si elle est élue.</p>
                        <p className="menu">Envoyez votre prénom et votre recette à : <a href="mailto:lagarennepizza@gmail.com">lagarennepizza@gmail.com</a></p>
                    </div>
                </Section>
            </Content>
            <Footer />
        </>
    )
}

Layout.propTypes = {
    children: PropTypes.node.isRequired
}

export default Layout
