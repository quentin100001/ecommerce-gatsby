---
pizza_name: 4 Saison
pizza_ingredients: Tomate, mozzarella, cœur d'artichauts, poivrons rouges,
  olives, champignons, jambon, roquette
pizza_price33: "11"
pizza_price46: "19"
pizza_category: Les classiques
---
