import PropTypes from "prop-types"
import React from "react"
import { NavItem } from "./nav/NavItem"
import { Nav } from "./nav/Nav"

export class Header extends React.Component {
    render() {
        return (
          <header>
              <h1 className="header__title">{this.props.title}</h1>
              <Nav>
                  <NavItem to="/">Accueil</NavItem>
                  <NavItem to="/location">Emplacement</NavItem>
                  <NavItem to="/menu">Menu</NavItem>
                  <NavItem to="/news">Dernières news</NavItem>
                  <NavItem to="/shop">Boutique</NavItem>
                  <NavItem to="/contact">Contact</NavItem>
              </Nav>
          </header>
        )
    }
}

Header.propTypes = {
    title: PropTypes.string.isRequired
}
