import { Header } from "../components/Header"
import React from "react"
import { Content } from "../components/Content"
import { Section } from "../components/Section"
import { Col } from "../components/Col"
import { Container } from "../components/Container"
import { Footer } from "../components/Footer"
import { graphql } from "gatsby"
import { PizzaCategory } from "../components/PizzaCategory"
import { Pizza } from "../components/Pizza"

export const pizza = graphql`
    query {
        pizza : allMarkdownRemark(filter: {fields: {sourceInstanceName: {eq: "pizza"}}}) {
            edges {
                node {
                    frontmatter {
                        pizza_category
                        pizza_ingredients
                        pizza_name
                        pizza_price33
                        pizza_price46
                    }
                }
            }
        }
        catgories: allMarkdownRemark(filter: {fields: {sourceInstanceName: {eq: "category"}}}) {
            edges {
                node {
                    frontmatter {
                        category_name
                    }
                }
            }
        }
    }
`;


const menu = ({data}) => {
    let result = [];
    let previous = [];
    for (let i = 0; i < data.catgories.edges.length; i++) {
        let categoryName = data.catgories.edges[i].node.frontmatter.category_name;
        previous.push(
          <PizzaCategory name={categoryName}>
              {data.pizza.edges.filter(edges => edges.node.frontmatter.pizza_category === categoryName).map((edges, index) =>
                <Pizza key={index}
                       name={edges.node.frontmatter.pizza_name}
                       ingredients={edges.node.frontmatter.pizza_ingredients}
                       price33={edges.node.frontmatter.pizza_price33}
                       price46={edges.node.frontmatter.pizza_price46}
                />
              )}
          </PizzaCategory>
        )
        if (previous.length >= 2 || i === data.catgories.edges.length - 1) {
            result.push(
              <Container className="menu__row">
                  <Col size={50}>
                      {previous[0]}
                  </Col>
                  <Col size={50}>
                      {previous[1] || ''}
                  </Col>
              </Container>
            );
            previous = [];
        }
    }

    return (
      <>
          <Header title="La Garenne Pizza" />
          <Content>
            <Section title="Menu" darkMode>
                {result}
            </Section>
          </Content>
          <Footer/>
      </>
    )
}

export default menu
