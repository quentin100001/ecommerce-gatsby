import React from "react"
import { Container } from "./Container"
import { Col } from "./Col"
import img from "../assets/img/pizzadrawn.png"
import PropTypes from "prop-types"
import { faTimes } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

export class ShopBasket extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            stack: this.props.stack
        }
    }

    onChange = (event) => {
        this.props.onChange(this.props, event.target.value)
        this.setState({
            stack: event.target.value
        })
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        nextState.stack = nextProps.stack;
        return true;
    }

    render() {
        return (
          <li className="shop__basket__item">
              <Container>
                  <Col size={25}>
                      <img src={this.props.img} alt="img" />
                  </Col>
                  <Col size={75}>
                      <div className="shop__basket__content">
                          <h4>{this.props.name}<span>{this.props.price*this.state.stack}€</span></h4>
                          <input className="input-text" type="number" min="0" max="10" name="quantity" value={this.state.stack} onChange={this.onChange}/>
                          <p>{this.props.size}</p>
                      </div>
                  </Col>
                  <FontAwesomeIcon icon={faTimes} onClick={() => this.props.onDelete(this.props)}/>
              </Container>
          </li>
        )
    }
}

ShopBasket.propTypes = {
    name: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    size: PropTypes.string.isRequired,
    stack: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
};

