import React from "react";
import { Col } from "./Col"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAt, faBullhorn, faClock, faMapMarker, faPhone, faUtensils } from "@fortawesome/free-solid-svg-icons"
import { Container } from "./Container"

export class Footer extends React.Component {
    render() {
        return (
          <footer>
              <Container>
                  <Col size={33}>
                      <div className="footer__col">
                          <h3><FontAwesomeIcon icon={faUtensils} /> A propos du camion</h3>
                          <p>Les pizzas cuisent devant vous !
                              Depuis 10 ans nous vous proposons un menu de 25 pizzas avec des ingrédients de qualité :
                              Oeufs de poules plein air, vraie mozzarella, merguez du boucher, basilic frais, farine italienne supérieure...
                          </p>
                      </div>
                  </Col>
                  <Col size={33}>
                      <div className="footer__col">
                          <h3><FontAwesomeIcon icon={faClock} /> Horaires</h3>
                          <ul className="footer__scheduler">
                              <li className="footer__scheduler_item">Lundi <span>Fermé</span></li>
                              <li className="footer__scheduler_item">Mardi <span>17h - 21h</span></li>
                              <li className="footer__scheduler_item">Mercredi <span>17h - 21h</span></li>
                              <li className="footer__scheduler_item">Jeudi <span>17h - 21h</span></li>
                              <li className="footer__scheduler_item">Vendredi <span>17h - 21h</span></li>
                              <li className="footer__scheduler_item">Samedi <span>17h - 21h</span></li>
                              <li className="footer__scheduler_item">Dimanche <span>Fermé</span></li>
                          </ul>
                      </div>
                  </Col>
                  <Col size={33}>
                      <div className="footer__col">
                          <h3><FontAwesomeIcon icon={faBullhorn} /> Contact</h3>
                          <ul className="footer__contact">
                              <li className="footer__contact__item"><FontAwesomeIcon icon={faMapMarker} /> 58 Avenue Foch,<div className="city">92250 La Garenne-Colombes</div></li>
                              <li className="footer__contact__item"><FontAwesomeIcon icon={faPhone} /> 06 68 06 12 34</li>
                              <li className="footer__contact__item"><FontAwesomeIcon icon={faAt} /> <a href="mailto:lagarennepizza@gmail.com">lagarennepizza@gmail.com</a></li>
                          </ul>
                      </div>
                  </Col>
              </Container>
          </footer>
        )
    }
}
