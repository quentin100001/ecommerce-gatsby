import React from "react"
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import { Header } from "../components/Header"
import { Content } from "../components/Content"
import { Footer } from "../components/Footer"
import { Section } from "../components/Section"
import * as Leaflet from "leaflet"

const location = () => {
    const center = [48.907, 2.2395];
    const position = [48.90955, 2.246887];
    const champPhilippe = [48.903875, 2.233967];
    if (typeof window !== 'undefined') {
        const icon = Leaflet.icon({
            iconUrl: "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png"
        })
        return (
          <>
              <Header title="La Garenne Pizza"/>
              <Content>
                  <Section title="Emplacement" className="location">
                      <Map center={center} zoom={15} >
                          <Marker position={position} icon={icon} >
                              <Popup className="location__popup">
                                  <h3>La Garenne Pizza</h3>
                                  <p>58 Avenue Foch, <br /> 92250 La Garenne-Colombes</p>
                                  <table>
                                      <tr><td>Lundi</td><td>Fermé</td></tr>
                                      <tr><td>Mardi</td><td>17h - 21h</td></tr>
                                      <tr><td>Mercredi</td><td>17h - 21h</td></tr>
                                      <tr><td>Jeudi</td><td>Fermé</td></tr>
                                      <tr><td>Vendredi</td><td>17h - 21h</td></tr>
                                      <tr><td>Samedi</td><td>17h - 21h</td></tr>
                                      <tr><td>Dimanche</td><td>Fermé</td></tr>
                                  </table>
                              </Popup>
                          </Marker>
                          <Marker position={champPhilippe} icon={icon} >
                              <Popup className="location__popup">
                                  <h3>La Garenne Pizza</h3>
                                  <p>Place des Champs-Philippe,<br /> 92250 La Garenne-Colombes</p>
                                  <table>
                                      <tr><td>Lundi</td><td>Fermé</td></tr>
                                      <tr><td>Mardi</td><td>Fermé</td></tr>
                                      <tr><td>Mercredi</td><td>Fermé</td></tr>
                                      <tr><td>Jeudi</td><td>17h - 21h</td></tr>
                                      <tr><td>Vendredi</td><td>Fermé</td></tr>
                                      <tr><td>Samedi</td><td>Fermé</td></tr>
                                      <tr><td>Dimanche</td><td>Fermé</td></tr>
                                  </table>
                              </Popup>
                          </Marker>
                          <TileLayer
                            url="http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"
                            // url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            // attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                          />
                      </Map>
                  </Section>
              </Content>
              <Footer/>
          </>
        )
    }else{
        return null
    }
}

export default location
