import React from "react";

export class Gallery extends React.Component {
    render() {
        return (
          <div className="gallery">
              {this.props.children}
          </div>
        )
    }
}
