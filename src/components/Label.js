import * as React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import PropTypes from "prop-types"

export class Label extends React.Component{
    render() {
        return (
          <div className="label">
              <FontAwesomeIcon icon={this.props.icon}/> <spawn>{this.props.msg}</spawn>
          </div>
        )
    }
}

Label.propTypes = {
    icon: PropTypes.any.isRequired,
    msg: PropTypes.string.isRequired
};
