import React from "react"
import img from "../assets/img/pizzadrawn.png"
import Tooltip from "react-tooltip-lite"
import Button from "./Button"
import PropTypes from "prop-types"

export class ShopItem extends React.Component{
    render() {
        return (
          <div className="shop__item">
              <img src={img} alt="pizza" />
              <h4>{this.props.name}</h4>
              <p className="ingredients">{this.props.ingredients}</p>
              <Tooltip className="shop__item__button" background="#333" content="Ajouter au panier">
                  <Button onClick={() => this.props.onSelect({...this.props,size:"medium",price: this.props.price33})} type="primary" className="shop__item__button" linkTo="">Moyenne - {this.props.price33}</Button>
              </Tooltip>
              <Tooltip className="shop__item__button" background="#333" content="Ajouter au panier">
                  <Button onClick={() => this.props.onSelect({...this.props,size:"large",price: this.props.price46})} type="primary" linkTo="">Large - {this.props.price46}</Button>
              </Tooltip>
          </div>
        )
    }
}

ShopItem.propTypes = {
    name: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    price33: PropTypes.string.isRequired,
    price46: PropTypes.string.isRequired,
    ingredients: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired
};

ShopItem.defaultProps = {
    onSelect: ()=>{}
}
