import React from "react"
import img from "../assets/img/IMG-20190623-WA0006.png"
import { faCalendar } from "@fortawesome/free-solid-svg-icons"
import { Label } from "./Label"
import PropTypes from "prop-types"

export class News extends React.Component {
    render() {
        return (
          <div className="news">
              <div className="news_thumbnails">
                  <img src={this.props.img} alt="img"/>
              </div>
              <div className="news__core">
                  <Label icon={faCalendar} msg="5 Mai 2020"/>
                  <h3>Les pizzas du mois de mai</h3>
                  <p>
                      <div dangerouslySetInnerHTML={{ __html: this.props.text.replace(/\\/g, '<br />') }} />
                  </p>
              </div>
          </div>
        )
    }
}

News.propTypes = {
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired
};
