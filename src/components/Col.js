import React from "react";
import PropTypes from "prop-types"

export class Col extends React.Component {
    render() {
        return (
          <div className={`container__col container__col-${this.props.size} ${this.props.className}`}>
              {this.props.children}
          </div>
        )
    }
}

Col.propTypes = {
    size: PropTypes.number.isRequired,
    className: PropTypes.string
}

Col.defaultProps = {
    className: ""
}

