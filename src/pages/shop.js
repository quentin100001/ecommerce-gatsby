import React, { useState } from "react"
import { Header } from "../components/Header"
import { Footer } from "../components/Footer"
import { Content } from "../components/Content"
import { Section } from "../components/Section"
import { ShopItem } from "../components/ShopItem"
import img from "../assets/img/pizzadrawn.png"
import { graphql, navigate } from "gatsby"
import { Col } from "../components/Col"
import { Container } from "../components/Container"
import { ShopBasket } from "../components/ShopBasket"
import Button from "../components/Button"
import { faChevronRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

export const pizza = graphql`
    query {
        pizza : allMarkdownRemark(filter: {fields: {sourceInstanceName: {eq: "pizza"}}}) {
            edges {
                node {
                    frontmatter {
                        pizza_category
                        pizza_ingredients
                        pizza_name
                        pizza_price33
                        pizza_price46
                    }
                }
            }
        }
    }
`

const Shop = ({ data }) => {
    const defaultBracket = (typeof window !== 'undefined' ? (localStorage.getItem('bracket') ? JSON.parse(localStorage.getItem('bracket')) : {}) : {});
    const [bracket, setBracket] = useState(defaultBracket);

    const addBracket = (pizza, stack) => {
        let newBracket = {...bracket};
        if (newBracket[pizza.name+pizza.size]) {
            newBracket[pizza.name+pizza.size].stack = parseInt(stack || newBracket[pizza.name+pizza.size].stack+1);
            newBracket[pizza.name+pizza.size].stack = newBracket[pizza.name+pizza.size].stack > 10 ? 10 : newBracket[pizza.name+pizza.size].stack;
            if (newBracket[pizza.name + pizza.size].stack === 0)
                delete newBracket[pizza.name+pizza.size];
        }else{
            newBracket[pizza.name+pizza.size] = pizza;
            newBracket[pizza.name+pizza.size].stack = 1;
        }
        if (typeof window !== 'undefined') localStorage.setItem('bracket', JSON.stringify(newBracket));
        setBracket(newBracket)
    }

    const deleteBracket = (pizza) => {
        let newBracket = {...bracket};
        delete newBracket[pizza.name+pizza.size];
        if (typeof window !== 'undefined') localStorage.setItem('bracket', JSON.stringify(newBracket));
        setBracket(newBracket)
    }

    let total = 0;

    return (
      <>
          <Header title="La Garenne Pizza"/>
          <Content>
              <Section title="Commander en ligne" darkMode={false}>
                  <Container>
                      <Col size={75}>
                          {data.pizza.edges.map((edges, index) =>
                            <ShopItem img={img}
                                      price33={edges.node.frontmatter.pizza_price33}
                                      price46={edges.node.frontmatter.pizza_price46}
                                      name={edges.node.frontmatter.pizza_name}
                                      ingredients={edges.node.frontmatter.pizza_ingredients}
                                      onSelect={addBracket}
                                      key={index}
                            />
                          )}
                      </Col>
                      <Col size={25} className="shop__basket">
                          <h3 className="shop__basket__title">Panier</h3>
                          <ul>
                              {Object.values(bracket).map((value, index) => {
                                  total += value.price*value.stack;
                                  return <ShopBasket key={index} name={value.name} img={value.img} price={value.price} size={value.size} stack={value.stack} onChange={addBracket} onDelete={deleteBracket}/>
                                }
                              )}
                          </ul>
                          <p className="shop__basket__total">Total : <span>{total}€</span></p>
                          <Button type="primary" className="float-right" onClick={() => navigate("/checkout")}>Commander <FontAwesomeIcon icon={faChevronRight}/></Button>
                      </Col>
                  </Container>
              </Section>
          </Content>
          <Footer/>
      </>
    )
}

export default Shop
