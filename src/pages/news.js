import { Header } from "../components/Header"
import { Content } from "../components/Content"
import React from "react"
import { Footer } from "../components/Footer"
import { Section } from "../components/Section"
import { News } from "../components/News"
import { Container } from "../components/Container"
import { Col } from "../components/Col"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFileAlt } from "@fortawesome/free-solid-svg-icons"
import { graphql } from "gatsby"
import unified from "unified";
import markdown from "remark-parse"
import html from "remark-html"

export const data = graphql`
    query {
        news: allMarkdownRemark(filter: {fields: {sourceInstanceName: {eq: "news"}}}) {
            edges {
                node {
                    frontmatter{
                        news_title
                        news_image
                        news_description
                    }
                }
            }
        }
    }
`;

const news = ({data}) => {
    console.log(data)
    return (
      <>
          <Header title="La Garenne Pizza" />
          <Content>
            <Section title="Dernières news" darkMode={false}>
                <Container>
                    <Col size={66}>
                        {data.news.edges.map((edges, index) =>
                            <News key={index}
                                  title={edges.node.frontmatter.news_title}
                                  date={edges.node.frontmatter.news_title}
                                  text={unified().use(markdown).use(html).processSync(edges.node.frontmatter.news_description).toString()}
                                  img={edges.node.frontmatter.news_image}
                            />
                        )}
                    </Col>
                    <Col size={33} className="news__summary">
                        <h3>Dernières news</h3>
                        <ul className="news__summary__list">
                            {data.news.edges.map((edges, index) =>
                              <li key={index}><FontAwesomeIcon icon={faFileAlt}/> {edges.node.frontmatter.news_title}</li>
                            )}
                        </ul>
                    </Col>
                </Container>
            </Section>
          </Content>
          <Footer />
      </>
    )
}

export default news
