import React from "react";

export class Nav extends React.Component {
    render() {
        return (
          <nav className="header__nav">
              <ul className="nav__list">
                  {this.props.children}
              </ul>
          </nav>
        )
    }
}
