import React from "react";
import PropTypes from "prop-types"
import before from "../assets/img/dash-before.png";
import after from "../assets/img/dash-after.png";
import dark_before from "../assets/img/dark-dash-before.png";
import dark_after from "../assets/img/dark-dash-after.png";

export class Section extends React.Component{
    render() {
        let noBorder = this.props.noBorder ? "content__section__noPadding" : "";
        let darkMode  = !this.props.darkMode ? "content__section__white" : "";
        return (
          <section className={`content__section ${noBorder} ${darkMode} ${this.props.className}`}>
              <div className="section__title">
                  <img src={this.props.darkMode ? before : dark_before} alt="before" />
                  <h2>{this.props.title}</h2>
                  <img src={this.props.darkMode ? after : dark_after} alt="after" />
              </div>
              <div className="section__core">
                  {this.props.children}
              </div>
          </section>
        )
    }
}

Section.propTypes = {
    title: PropTypes.string.isRequired,
    noBorder: PropTypes.bool,
    darkMode: PropTypes.bool,
    className: PropTypes.string
};

Section.defaultProps = {
    noBorder: false,
    darkMode: true,
    className: ""
}
